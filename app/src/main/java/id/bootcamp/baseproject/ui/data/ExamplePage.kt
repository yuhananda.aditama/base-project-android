package id.bootcamp.baseproject.ui.data

data class ExamplePage<T>(
    //T adalah generic class, yang mana classnya akan ditentukan ketika data ini dibuat
    var page: Int = 0,
    var totalPage: Int = 0,
    var data: T
)