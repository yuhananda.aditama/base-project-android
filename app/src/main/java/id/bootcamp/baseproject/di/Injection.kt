package id.bootcamp.baseproject.di

import android.content.Context
import id.bootcamp.baseproject.data.ExampleRepository
import id.bootcamp.baseproject.data.local.ExampleSharedPreference
import id.bootcamp.baseproject.data.local.room.ExampleDatabase
import id.bootcamp.baseproject.data.remote.retrofit.ApiConfig

object Injection {
    fun provideExampleRepository(context: Context):ExampleRepository{
        val exampleApiService = ApiConfig.getRegresApiService()
        val exampleDatabase = ExampleDatabase.getInstance(context)
        val exampleDao = exampleDatabase.exampleDao()
        val exampleSharedPreference = ExampleSharedPreference(context)
        return ExampleRepository.getInstance(exampleApiService,exampleDao,exampleSharedPreference)
    }
}