package id.bootcamp.baseproject.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import id.bootcamp.baseproject.data.local.entity.ExampleEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.util.concurrent.Executors

@TypeConverters(DateConverter::class)
@Database(entities = [ExampleEntity::class], version = 3, exportSchema = false)
abstract class ExampleDatabase : RoomDatabase() {

    abstract fun exampleDao(): ExampleDao

    companion object {
        @Volatile
        private var instance: ExampleDatabase? = null
        fun getInstance(context: Context): ExampleDatabase =
            instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context.applicationContext,
                    ExampleDatabase::class.java, "example.db"
                )
                    //.createFromAsset("database/example.db")
                    .addMigrations(migrations)
                    .build()
            }

        val migrations = object : Migration(2,3) {
            override fun migrate(db: SupportSQLiteDatabase) {
            }
        }
    }
}