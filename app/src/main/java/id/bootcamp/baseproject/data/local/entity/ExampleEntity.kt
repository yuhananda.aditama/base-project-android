package id.bootcamp.baseproject.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "example")
class ExampleEntity(

    @field:ColumnInfo(name = "id")
    @field:PrimaryKey
    var id: Int = 0,

    @field:ColumnInfo(name = "title")
    var title: String = "",

    @field:ColumnInfo(name = "desc")
    var desc: String = ""
)